# Icinga Ansible Role

This is an [Ansible](https://docs.ansible.com/ansible/latest/) role for
managing [Icinga](https://github.com/icinga/) server, SSH agent and web-interface
resources.
