# Copyright (c) 2018-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://www.vagrantup.com/docs/vagrantfile/vagrant_version.html
Vagrant.require_version '>= 2.0.0'

# https://docs.ansible.com/ansible/latest/reference_appendices/config.html#the-configuration-file
ENV['ANSIBLE_CONFIG'] ||= File.join(__dir__, '.ansible.cfg')

# https://www.vagrantup.com/docs/provisioning/ansible_common.html#groups
ANSIBLE_GROUPS = {
  'icinga-servers' => [
    'server-buster.test',
  ],
  'icinga-web-servers' => [
    'web-buster.test',
  ],
}.freeze

# Used in the Icinga Web service to connect to the Icinga server.
# Within Vagrant this is easier than using the host name, which would require
# setting up DNS or maintaining /etc/hosts files to resolve properly.
ICINGA_SERVER_IP = '10.8.10.10'.freeze

# Used in the Icinga server's pg_hba.conf file to grant database access to
# the Icinga Web server. Within Vagrant this is easier than using the host
# name, which would require setting up both DNS and reverse DNS otherwise.
ICINGAWEB_SERVER_IP = '10.8.10.8'.freeze

# https://www.vagrantup.com/docs/provisioning/ansible.html
# https://icinga.com/docs/icinga2/latest/doc/07-agent-based-monitoring/
def provision_as_ssh_agent(config, ip)
  config.vm.provision('ansible') do |ansible|
    ansible.compatibility_mode = '2.0'
    ansible.extra_vars = {
      'icinga_ssh_agent_server_associations' => [{
        'name' => 'server-buster.test',
        'user' => 'nagios',
      }],
      'icinga_ssh_agent_ipv4_address' => ip,
      'icinga_ssh_agent_plugin_directory' => '/usr/lib/nagios/plugins',
    }
    ansible.groups = ANSIBLE_GROUPS
    ansible.playbook = File.join(__dir__, 'provision-icinga-ssh-agents.yml')
  end
end

# https://icinga.com/docs/icinga2/latest/
def provision_as_server(config)
  config.vm.provision('ansible') do |ansible|
    ansible.compatibility_mode = '2.0'
    ansible.extra_vars = {
      'icinga_server_database_name' => 'icinga',
      'icinga_server_database_password' => 'changeme',
      'icinga_server_database_user' => 'icinga',
      'icinga_server_http_service_group' => [
        'icinga-web-servers',
      ],
      'icinga_http_response_time_check_group' => [
        'icinga-web-servers',
      ],
      'icinga_https_response_time_check_group' => [
        'icinga-web-servers',
      ],
      'icinga_ssl_cert_check_group' => [
        'icinga-web-servers',
      ],
      'icinga_http_port_check_group' => [
        'icinga-web-servers',
      ],
      'icinga_https_port_check_group' => [
        'icinga-web-servers',
      ],
      'icinga_web_server_address' => ICINGAWEB_SERVER_IP,
    }
    ansible.groups = ANSIBLE_GROUPS
    ansible.playbook = File.join(__dir__, 'provision-icinga-servers.yml')
  end
end

# https://icinga.com/docs/icingaweb2/latest/
def provision_as_web_server(config)
  config.vm.provision('ansible') do |ansible|
    ansible.compatibility_mode = '2.0'
    ansible.extra_vars = {
      'icinga_server_name' => 'server-buster.test',
      'icinga_server_ip' => ICINGA_SERVER_IP,
      'icinga_web_database_name' => 'icingaweb',
      'icinga_web_database_password' => 'changeme',
      'icinga_web_database_user' => 'icingaweb',
    }
    ansible.groups = ANSIBLE_GROUPS
    ansible.playbook = File.join(__dir__, 'provision-icinga-web-servers.yml')
  end
end

# https://www.vagrantup.com/docs/vagrantfile/version.html
Vagrant.configure(2) do |vagrant|

  # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
  vagrant.vm.box_check_update = false
  vagrant.vm.post_up_message = nil

  # https://www.vagrantup.com/docs/synced-folders/basic_usage.html#disabling
  vagrant.vm.synced_folder('.', '/vagrant', disabled: true)

  # https://docs.ruby-lang.org/en/2.6.0/Module.html#method-i-define_method
  define_method(:define_vm) do |fqdn, ip, &block|

    # https://www.vagrantup.com/docs/multi-machine/
    vagrant.vm.define(fqdn) do |config|
      config.vm.hostname = fqdn
      config.vm.network(:private_network, ip: ip)
      block.call(config)
      provision_as_ssh_agent(config, ip)
    end

  end

  # https://wiki.debian.org/DebianBuster
  define_vm('server-buster.test', ICINGA_SERVER_IP) do |config|
    config.vm.box = 'debian/buster64'
    provision_as_server(config)
  end

  # https://www.vagrantup.com/docs/multi-machine/
  define_vm('web-buster.test', ICINGAWEB_SERVER_IP) do |config|
    config.vm.box = 'debian/buster64'
    provision_as_web_server(config)
  end

  # https://wiki.centos.org/Manuals/ReleaseNotes/CentOS7.1908
  # TODO maybe get rid of this as redundant and hopeless?
  define_vm('ssh-agent-centos7.test', '10.8.10.38') do |config|
    config.vm.box = 'centos/7'
  end

  # https://wiki.debian.org/DebianJessie
  define_vm('ssh-agent-jessie.test', '10.8.10.48') do |config|
    config.vm.box = 'debian/jessie64'
  end

  # https://wiki.debian.org/DebianStretch
  define_vm('ssh-agent-stretch.test', '10.8.10.28') do |config|
    config.vm.box = 'debian/stretch64'
  end

end
